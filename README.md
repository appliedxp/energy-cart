# README #

This README provides the necessary information to get the IGS Energy Cart software setup and running.

### What is this repository for? ###

* IGS Energy Cart control and game software
* Version 1.0
* [Applied Experience](https://appliedxp.com)

### How do I get set up? ###

1. Download using the "Download repository" link in the Downloads section  
2. Unzip the package and place the file "Energy_Game.desktop" on the Raspberry Pi desktop  
3. All remaining files must be placed in /home/pi/Energy_Cart  
4. Enable the I2C interface:  
  Preferences -> Raspberry Pi Configuration -> "Interfaces" tab  
5. Make sure the system is connected to the internet, then open Terminal and run the following command:  
  ```
  $ sudo bash /Energy_Cart/setup.sh
  ```  
  This will download and install the necessary dependencies  
6. To disable the onboard WiFi module, add this line to /boot/config.txt:  
  ```
  dtoverlay=pi3-disable-wifi
  ```  
  This is recommended when using the external USB WiFi module

### Who do I talk to? ###

* [matt@appliedxp.com](mailto:matt@appliedxp.com)
