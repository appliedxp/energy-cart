# ==============================
# Energy Demonstration Cart
#
# APPLIED EXPERIENCE LLC 2018
# ==============================

# Libraries
import time
import Adafruit_ADS1x15
from tkinter import *
import tkinter.font
import tkinter.ttk as ttk
import RPi.GPIO as GPIO

# GPIO setup
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(25, GPIO.OUT, initial=GPIO.HIGH)     # Battery charge mode
GPIO.setup(12, GPIO.IN)

# ADS1015 (12-bit) instance
adc1 = Adafruit_ADS1x15.ADS1015(address = 0x48, busnum = 1)     # ADDR tied to GND
adc2 = Adafruit_ADS1x15.ADS1015(address = 0x49, busnum = 1)     # ADDR tied to VDD
adc3 = Adafruit_ADS1x15.ADS1015(address = 0x4A, busnum = 1)     # ADDR tied to SDA

# Choose a gain of 1 for reading voltages from 0 to 4.09V.
# Or pick a different gain to change the range of voltages that are read:
#  - 2/3 = +/-6.144V
#  -   1 = +/-4.096V
#  -   2 = +/-2.048V
#  -   4 = +/-1.024V
#  -   8 = +/-0.512V
#  -  16 = +/-0.256V
# See table 3 in the ADS1015/ADS1115 datasheet for more info on gain.
adcGain = 1

# Constants
ADC_REF = 4.096
ADC_BITS = 2047         # 4095/2, since this ADC actually handles negative voltage as well
STD_V_SCALE = (22 + 1000) / 22      # resistor divider (R1 + R2) / R2
STD_I_SCALE = 200       # volts to mA

BAR_POWER_MAX = 10      # Power(W) which would put the bar graph at full scale
BAR_ENERGY_MAX = 0.4    # Energy(Wh) which would put the bar graph at full scale
PBAR_LENGTH = 300       # Height of vertical power/progress bar
EBAR_LENGTH = 340       # Length of horizontal energy/progress bar
GAMETIME = 59.99        # duration of the countdown
MAVG_SIZE = 20          # Size of moving average array for voltage/current measurements
ICAL_SAMPLES = 20       # Number of samples to take to calculate current offset
    

def readADC(module, channel):
    adc = module
    try:
        value = adc.read_adc(channel, gain = adcGain)
    except:
        value = 0
    return value

class Generator:
    def __init__(self, adcModule, adcChanV, adcChanI, vScaling, iScaling):
        self.index = 0
        self.voltage_now = [0] * MAVG_SIZE    # intantaneous readings (list/array)
        self.current_now = [0] * MAVG_SIZE    # intantaneous readings (list/array)
        self.voltage = 0            # averaged (smoothed) reading
        self.curent = 0             # averaged (smoothed) reading
        self.power = 0
        self.energy = 0
        self.adcModule = None
        self.adcChanV = None
        self.adcChanI = None
        self.adcModule = adcModule
        self.adcChanV = adcChanV
        self.adcChanI = adcChanI
        self.vScaling = vScaling
        self.iScaling = iScaling
        self.iOffset = 0
        self.elapTime = 0.00
        self.lastTime = time.time()

    def calibrate(self):
        self.iOffset = 0
        # take multiple current readings
        for i in range (0, (ICAL_SAMPLES - 1)):
            self.current_now[i] = self.iScaling*(readADC(self.adcModule, self.adcChanI))*(ADC_REF / ADC_BITS) - self.iOffset     # in mA
            time.sleep(0.01)
        # calculate average
        for n in range (0, (ICAL_SAMPLES - 1)):
            self.iOffset += self.current_now[n]/ICAL_SAMPLES

    def update(self):
        self.elapTime = time.time() - self.lastTime
        self.lastTime = time.time()
        self.voltage_now[self.index] = self.vScaling*(readADC(self.adcModule, self.adcChanV))*(ADC_REF / ADC_BITS)
        self.current_now[self.index] = self.iScaling*(readADC(self.adcModule, self.adcChanI))*(ADC_REF / ADC_BITS) - self.iOffset     # in mA
        if self.index >= (MAVG_SIZE - 1):
            self.index = 0      # reset index to zero (wraparound)
        else:
            self.index += 1     # increment index
        # calculate moving average
        self.voltage = 0
        self.current = 0
        for n in range (0, (MAVG_SIZE - 1)):
            self.voltage += max(0, (self.voltage_now[n]/MAVG_SIZE))     # keep results positive
            self.current += max(0, (self.current_now[n]/MAVG_SIZE))
        self.power = self.voltage*(self.current/1000)       # in W

    def updateenergy(self):
        self.energy += self.power*(self.elapTime/3600)      # in Wh

    def reset(self):
        # for resetting accumulated energy
        self.lastTime = time.time()
        self.energy = 0

# Create an instance for each generator
generator1 = Generator(adc2, 0, 1, STD_V_SCALE, STD_I_SCALE)
generator2 = Generator(adc2, 2, 3, STD_V_SCALE, STD_I_SCALE)
generator3 = Generator(adc1, 0, 1, STD_V_SCALE, STD_I_SCALE)
generator4 = Generator(adc1, 2, 3, STD_V_SCALE, STD_I_SCALE)


# Game timer and state control
class Timer(Frame):  
    # Implements a countdown timer frame widget                                                              
    def __init__(self, parent=None, **kw):        
        Frame.__init__(self, parent, kw)
        self._start = 0.0 
        self._countdowntime = GAMETIME
        self._elapsedtime = 0.0
        self._running = 0
        self.timestr = StringVar()               
        self.makeWidgets()
        GPIO.output(25, GPIO.HIGH)
        self.modestr = StringVar()
        self.modestr.set("Battery Charge Mode")
        self.reset = 0

    def makeWidgets(self):                         
        # Make the time label
        timerFont = tkinter.font.Font(family = 'Helvetica', size = 48, weight = "bold")
        timeDisp = Label(self, font = timerFont, textvariable=self.timestr)
        self._setTime(self._countdowntime)
        timeDisp.pack(expand = YES, pady = 10, padx = 10)
        
    def _update(self): 
        # Update the label with countdown time
        self._elapsedtime = time.time() - self._start
        self._countdowntime = GAMETIME - self._elapsedtime
        if self._countdowntime > 0:
            self._setTime(self._countdowntime)
            self._timer = self.after(50, self._update)
        else:
            self._countdowntime = 0
            self.Stop()
    
    def _setTime(self, time):
        # Set the time string to Minutes:Seconds:Hundreths
        minutes = int(time/60)
        seconds = int(time - minutes*60.0)
        hseconds = int((time - minutes*60.0 - seconds)*100)                
        self.timestr.set('%02d:%02d' % (seconds, hseconds))
        
    def Start(self):                                                     
        # Start the timer, ignore if running
        if self._running == 0 and self._countdowntime > 0:
            self.modestr.set("Ready...")
            self._running = 3
            self._timer = self.after(1000, self.Start)

        elif self._running == 3:
            self.modestr.set("Set...")
            self._running = 2
            self._timer = self.after(1000, self.Start)

        elif self._running == 2:
            self.modestr.set("GO!")
            self.reset = 0
            GPIO.output(25, GPIO.LOW)
            self._start = time.time() - self._elapsedtime
            self._update()
            self._running = 1        
            
    def Stop(self):                                    
        # Stop the timer, ignore if stopped
        if self._running:
            self.after_cancel(self._timer)
            self._setTime(self._countdowntime)
            self._running = 0
            GPIO.output(25, GPIO.HIGH)
            #self.modestr.set("Team 1 Wins!")
            self.modestr.set("Battery Charge Mode")
    
    def Reset(self):                                  
        # Reset the timer and accumulated energy
        self.reset = 1
        self._start = time.time()
        self._elapsedtime = 0.0
        self._countdowntime = GAMETIME
        self._setTime(self._countdowntime)


        
# calibrate current sensors
generator1.calibrate()
generator2.calibrate()
generator3.calibrate()
generator4.calibrate()


# GUI setup
win = Tk()
win.title("Energy Cart - AXP 2018")
w = win.winfo_screenwidth()
h = win.winfo_screenheight()
win.geometry(("%dx%d" % (w,h)))
win.anchor("n")

# Font sizes
creditFont = tkinter.font.Font(family = 'Helvetica', size = 14)
smallFont = tkinter.font.Font(family = 'Helvetica', size = 14, weight = "bold")
medFont = tkinter.font.Font(family = 'Helvetica', size = 26, weight = "bold")
bigFont = tkinter.font.Font(family = 'Helvetica', size = 42, weight = "bold")

style = ttk.Style()
style.theme_use('clam')      # try 'default', 'alt', 'clam'

# Setup colors for each generator's "power meter"
style.configure("gen1.Vertical.TProgressbar", background = 'orange red')  # red
style.configure("gen2.Vertical.TProgressbar", background = 'dark orange')  # orange
style.configure("gen3.Vertical.TProgressbar", background = 'blue violet')  # green
style.configure("gen4.Vertical.TProgressbar", background = 'slate blue')  # purple
style.configure("team1.Horizontal.TProgressbar", background = 'red')  # orangered
style.configure("team2.Horizontal.TProgressbar", background = 'blue')  # blue

buttonrow = 0
statusrow = 0
gencol = 2
genrow = 2

# GUI widgets
mode = Label(win, font = bigFont, height = 1, width = 20)
mode.grid(row = statusrow, column = 2, columnspan = 4, pady = 10, sticky = W)

timer = Timer(win)
timer.grid(row = statusrow, column = 6)

#logo = PhotoImage(file=r"/home/pi/Energy_Cart/RawAXPLogo_small.gif")
img = tkinter.PhotoImage(file = r"/home/pi/Energy_Cart/RawAXPLogo_small.gif")
logo = tkinter.Label(win, image = img)
logo.photo = img
logo.grid(row = 5, column = 9, rowspan = 2, sticky = E)


# Buttons
startButton = tkinter.Button(win, font = smallFont, text = "Start Game", command = timer.Start)
startButton.grid(row = buttonrow, column = 9, ipadx = 10, ipady = 10, padx = 10, pady = 10, sticky = W+E)
stopButton = tkinter.Button(win, font = smallFont, text = "Stop Game", command = timer.Stop)
stopButton.grid(row = buttonrow+1, column = 9, ipadx = 10, ipady = 10, padx = 10, pady = 10, sticky = W+E)
resetButton = tkinter.Button(win, font = smallFont, text = "Reset", command = timer.Reset)
resetButton.grid(row = buttonrow+2, column = 9, ipadx = 10, ipady = 10, padx = 10, pady = 10, sticky = W+E)
quitButton = tkinter.Button(win, font = smallFont, text = "Quit", command = win.destroy)
quitButton.grid(row = buttonrow+3, column = 9, ipadx = 10, ipady = 10, padx = 10, pady = 10, sticky = W+E)


# Team 1
team1name = Label(win, font = bigFont, height = 1, width = 12)
team1name.grid(row = genrow-1, column = gencol, columnspan = 2, pady = 30)
team1name.configure(text = "Team 1")
team1energy = Label(win, font = medFont, height = 1, width = 10)
team1energy.grid(row = genrow, column = gencol, columnspan = 2)
team1bar = ttk.Progressbar(win, style = "team1.Horizontal.TProgressbar", orient = "horizontal", length = EBAR_LENGTH, mode = "determinate")
team1bar.grid(row = genrow+1, column = gencol, columnspan = 2, padx = 40, pady = 5, sticky = N+S)

# Gen 1
gen1power = Label(win, font = medFont, height = 1, width = 10)
gen1power.grid(row = genrow+2, column = gencol, pady = 20)
gen1bar = ttk.Progressbar(win, style = "gen1.Vertical.TProgressbar", orient = "vertical", length = PBAR_LENGTH, mode = "determinate")
gen1bar.grid(row = genrow+3, column = gencol, padx = 100, sticky = W+E+N+S)
gen1detail = Label(win, font = smallFont, height = 1, width = 14, pady = 5)
gen1detail.grid(row = genrow+4, column = gencol)
gen1name = Label(win, font = medFont, height = 1, width = 12)
gen1name.grid(row = genrow+5, column = gencol, padx = 20)
gen1name.configure(text = "Generator 1")

# Gen 2
gen2power = Label(win, font = medFont, height = 1, width = 10)
gen2power.grid(row = genrow+2, column = gencol+1, pady = 20)
gen2bar = ttk.Progressbar(win, style = "gen2.Vertical.TProgressbar", orient = "vertical", length = PBAR_LENGTH, mode = "determinate")
gen2bar.grid(row = genrow+3, column = gencol+1, padx = 100, sticky = W+E+N+S)
gen2detail = Label(win, font = smallFont, height = 1, width = 14, pady = 5)
gen2detail.grid(row = genrow+4, column = gencol+1)
gen2name = Label(win, font = medFont, height = 1, width = 12)
gen2name.grid(row = genrow+5, column = gencol+1, padx = 20)
gen2name.configure(text = "Generator 2")

# Add some space between each team
win.grid_columnconfigure(4, minsize = 200)

# Team 2
team2name = Label(win, font = bigFont, height = 1, width = 12)
team2name.grid(row = genrow-1, column = gencol+3, columnspan = 2, pady = 30)
team2name.configure(text = "Team 2")
team2energy = Label(win, font = medFont, height = 1, width = 10)
team2energy.grid(row = genrow, column = gencol+3, columnspan = 2)
team2bar = ttk.Progressbar(win, style = "team2.Horizontal.TProgressbar", orient = "horizontal", length = EBAR_LENGTH, mode = "determinate")
team2bar.grid(row = genrow+1, column = gencol+3, columnspan = 2, padx = 40, pady = 5, sticky = N+S)

# Gen 3
gen3power = Label(win, font = medFont, height = 1, width = 10)
gen3power.grid(row = genrow+2, column = gencol+3, pady = 20)
gen3bar = ttk.Progressbar(win, style = "gen3.Vertical.TProgressbar", orient = "vertical", length = PBAR_LENGTH, mode = "determinate")
gen3bar.grid(row = genrow+3, column = gencol+3, padx = 100, sticky = W+E+N+S)
gen3detail = Label(win, font = smallFont, height = 1, width = 14, pady = 5)
gen3detail.grid(row = genrow+4, column = gencol+3)
gen3name = Label(win, font = medFont, height = 1, width = 12)
gen3name.grid(row = genrow+5, column = gencol+3, padx = 20)
gen3name.configure(text = "Generator 3")

# Gen 4
gen4power = Label(win, font = medFont, height = 1, width = 10)
gen4power.grid(row = genrow+2, column = gencol+4, pady = 20)
gen4bar = ttk.Progressbar(win, style = "gen4.Vertical.TProgressbar", orient = "vertical", length = PBAR_LENGTH, mode = "determinate")
gen4bar.grid(row = genrow+3, column = gencol+4, padx = 100, sticky = W+E+N+S)
gen4detail = Label(win, font = smallFont, height = 1, width = 14, pady = 5)
gen4detail.grid(row = genrow+4, column = gencol+4)
gen4name = Label(win, font = medFont, height = 1, width = 12)
gen4name.grid(row = genrow+5, column = gencol+4, padx = 20)
gen4name.configure(text = "Generator 4")

# AXP credit at bottom
axpName = Label(win, font = creditFont, height = 1, width = 20)
axpName.grid(row = genrow+6, column = 0, pady = 80, columnspan = 3, sticky = S+W)
axpName.configure(text = "Applied Experience LLC")


def UpdateGenerators():
    generator1.update()
    gen1power.configure(text = "%.1f W" % generator1.power)
    gen1detail.configure(text = "%.1f V  %.1f mA" % (generator1.voltage, generator1.current))
    gen1bar["value"] = int((generator1.power*100)/BAR_POWER_MAX)

    generator2.update()
    gen2power.configure(text = "%.1f W" % generator2.power)
    gen2detail.configure(text = "%.1f V  %.1f mA" % (generator2.voltage, generator2.current))
    gen2bar["value"] = int((generator2.power*100)/BAR_POWER_MAX)

    generator3.update()
    gen3power.configure(text = "%.1f W" % generator3.power)
    gen3detail.configure(text = "%.1f V  %.1f mA" % (generator3.voltage, generator3.current))
    gen3bar["value"] = int((generator3.power*100)/BAR_POWER_MAX)

    generator4.update()
    gen4power.configure(text = "%.1f W" % generator4.power)
    gen4detail.configure(text = "%.1f V  %.1f mA" % (generator4.voltage, generator4.current))
    gen4bar["value"] = int((generator4.power*100)/BAR_POWER_MAX)

def UpdateEnergy():
    generator1.updateenergy()
    generator2.updateenergy()
    generator3.updateenergy()
    generator4.updateenergy()
    
    team1energy.configure(text = "%.1f mWh" % ((generator1.energy + generator2.energy)*1000))  # display in mWh
    team1bar["value"] = int(((generator1.energy + generator2.energy)*100)/BAR_ENERGY_MAX)

    team2energy.configure(text = "%.1f mWh" % ((generator3.energy + generator4.energy)*1000))  # display in mWh
    team2bar["value"] = int(((generator3.energy + generator4.energy)*100)/BAR_ENERGY_MAX)



while 1:
    # check for timer reset status, also reset acculutated energy for each generator
    if timer.reset == 1:
        generator1.reset()
        generator2.reset()
        generator3.reset()
        generator4.reset()
        UpdateEnergy()
    
    mode.configure(text = "%s" % timer.modestr.get())
    UpdateGenerators()
    if timer._countdowntime > 0 and timer._running == 1:
        UpdateEnergy()
        
    win.update_idletasks()
    win.update()
    time.sleep(0.01)
    


