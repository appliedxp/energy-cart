#!/.bin/bash
echo Energy Cart Setup
echo ...

apt-get update
apt-get install build-essential python-dev python-smbus git
cd ~
git clone https://github.com/adafruit/Adafruit_Python_ADS1x15.git
cd Adafruit_Python_ADS1x15
python3 setup.py install